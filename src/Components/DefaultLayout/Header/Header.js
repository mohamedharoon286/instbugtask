import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import Routes from "./Routes";
import "./Header.css";
import ProfilePhoto from "../../../assests/Images/seb.jpg"
const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  return (
    <>
      <Navbar  dark expand="md">
        <NavbarBrand to="/" className = "pl-2">LOGO</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="routes_list w-50 d-flex justify-content-around" navbar>
            {Routes.map(element => {
              return (
                <NavItem key={`${element.title}Id`}>
                  <NavLink to={`${element.Url}`}>{element.title}</NavLink>
                </NavItem>
              )
            })}
          </Nav>
        </Collapse>
      </Navbar>
      <div className="main_photo_container">
        <Row className="justify-content-around py-4 container-fluid align-items-center ">
            <Col xs={"12"} sm={"12"} md="5" lg="5">
              <h1 className="py-3 text-white font-weight-light"> Hey , I'm Mohamed Haroon.</h1>
              <h2 className="text-muted font-weight-light"> Front end developer from Cairo , Egypt . I develope websites to help business do better online. </h2>
            </Col>
            <Col xs={"12"} sm={"12"} md="3" lg="3" className="img_container text-center">
              <img className="profile_photo" src="Mohamed Ahmed Ali Haroon" title="Mohamed Ahmed Ali Haroon" src={ProfilePhoto} />
            </Col>
          </Row>
      </div>
    </>
  )
}
export default Header; 