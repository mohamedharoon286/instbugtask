const Routes = [
    {
        title: "Projects",
        Url: "/Projects",
    },
    {
        title: "About",
        Url: "/About",
    },
    {
        title: "Process",
        Url: "/Process",
    },
    {
        title: "Services",
        Url: "/Services",
    },
    {
        title: "Contact",
        Url: "/Contact",
    },
    {
        title: "Blog",
        Url: "/Blog",
    },
];
export default Routes;